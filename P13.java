import java.util.*;
public class P13 {
	public static double[][]  deg (double[][] a,int n,int m) {
		double[][] c= new double[n][n];
		double[][] b= new double[n][n];
		b=a;
		if (m!=1) {
			for (int k=2;k<m+1;k++) {
				for (int i=0;i<n;i++) {
					for (int j=0;j<n;j++) {
						c[i][j]=0;
						for (int p=0;p<n;p++) {
							c[i][j]+=b[i][p]*a[p][j];
						}
					}
				}
				b=c;
			}
		} else {c=a;}
		return c;
	}
	public static double[][] op(double [][] a, int n) {
		int k=1;
		int fc=1;
		double eps=1e-6;
		double maxb=eps;
		double[][] b= new double[n][n];
		double[][] c= new double[n][n];
		while (maxb>eps) {
			fc*=k;
			b=deg(a,n,k);
			for (int i=0; i<n;i++){
				for (int j=0;j<n;j++) {
					b[i][j]/=fc; 
					c[i][j]+=b[i][j];
				}
			}
			maxb=b[0][0];
			for (int i=0; i<n;i++){
				for (int j=0;j<n;j++) {
					if (b[i][j]>maxb) {maxb=b[i][j];} else {} 
				}
			}
			k++;
		}
		return c;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int n = sc.nextInt();
		double[][] a= new double[n][n];
		for (int i=0;i<n;i++) {
			for (int j=0;j<n;j++) {
				a[i][j] = sc.nextInt();
			}
		}
		double[][] x= new double[n][n];
		x=op(a,n);
		for (int i=0;i<n;i++){
			for (int j=0;j<n;j++) {
				System.out.print(x[i][j]+" ");
			}
			System.out.println();
		}
	}
}