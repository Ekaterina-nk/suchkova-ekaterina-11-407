import java.util.*;
public class P5{
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int n = sc.nextInt();
		long f=1;
		if (n%2==0) {
			for (int i=1;i<=n/2;i++) {
				f*=2*i;
				}
			} else {
				for (int i=0;i<=n/2;i++) {
				f*=2*i+1;
				}
			}
		System.out.println(f);
}
}