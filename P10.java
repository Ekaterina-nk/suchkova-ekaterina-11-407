import java.util.*;

public class P10
{
	public static void main (String [] args)
	{
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int[][] a = new int[n][n];
		for( int i=0;i<n;i++) {
			for(int j=0;j<n;j++) {
				a[i][j]= sc.nextInt();
				}
			}
			int[]str = new int[n];
			int[]stb = new int[n];
			int d1=0; 
			int d2=0;
			for( int i=0;i<n;i++) {
				for(int j=0;j<n;j++) {
				str[i]+=a[i][j];
				stb[j]+=a[j][i];
				if (i==j) {d1+=a[i][j];}
				if (i+j==n-1) {d2+=a[i][j];}
				}
			}
			boolean f=false;
			for (int i=0;i<n;i++) {
			if ((str[i]==stb[i])&(d1==d2)&(str[i]==d1)) {f=true;} else {f=false;}
			}
			if (f==true) {System.out.println("YES");} else {System.out.println("NO");}
	}
}